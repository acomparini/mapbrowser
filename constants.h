#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DEG_RAD(a) (M_PI / 180.) * (a)
#define RAD_DEG(a) (180. / M_PI) * (a)
#define RADIUS 6378137
#define TILESZ 256
#define lonmin 180
#define latmin 85.0511

int Longitude2TileX(double Longitude, int nZoom);
int Latitude2TileY(double Latitude, int nZoom);
double TileX2Longitude(double X, int nZoom);
double TileY2Latitude(double Y, int nZoom);

#endif