#include "mainwindow.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QScrollBar>
#include <QPaintEngine>
#include <Qdir>
#include <QfileInfo>
#include <QtOpenGL/QGLWidget>
#include <QWheelEvent>

#include "map_browser.h"
#define  _USE_MATH_DEFINES
#include <math.h>
#include "constants.h"

/*************************************************************************/

int Longitude2TileX(double Longitude, int nZoom)
{ 
	double pixelX = 256 * pow(2., nZoom) * ((Longitude + 180) / 360);
	int tileX = (int) floor(pixelX / 256);
	return tileX;
}
int Latitude2TileY(double Latitude, int nZoom)
{ 
	double sinLatitude = sin(DEG_RAD(Latitude));
	double pixelY = 256 * pow(2.0, nZoom) * (0.5 - log((1 + sinLatitude) / (1. - sinLatitude)) / (4 * M_PI));
	int tileY = (int) floor(pixelY / TILESZ);
	return tileY;
}
double TileX2Longitude(double X, int nZoom)
{
	return 360. * X / pow(2.0, nZoom) - 180;
}
double TileY2Latitude(double Y, int nZoom)
{
	double n = M_PI - 2.0 * M_PI * Y / pow(2.0, nZoom);
	return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
}
double asinh(double x)
{
	return log(x + sqrt(1 + x * x));
}
/***********************************************************************/
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	map_browser view;
	//view.setDragMode(QGraphicsView::ScrollHandDrag);

    int dx = 600;		// view x size
    int dy = 600;		// view y size
    view.resize(dx, dy);

	double x0 = 11.2;	// center screen longitude
	double y0 = 43.72;	// center screen latitude
    int zm = 11;			// tile scale factor (0 - 19)

    view.set_origin(x0, y0);
    view.set_scale(zm);


//    view.get_tile_list();
//    view.update();
    view.show();
    //view.scale(0.5, 0.5);

	//view.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//view.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    return a.exec();
}
/******************************************************************/

