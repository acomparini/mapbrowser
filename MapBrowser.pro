#-------------------------------------------------
#
# Project created by QtCreator 2014-03-15T16:18:51
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MapBrowser
TEMPLATE = app


SOURCES +=	main.cpp \
		map_browser.cpp \
		tile_item.cpp \
		tile_map.cpp


HEADERS +=	constants.h \
		map_browser.h \	
		tile_item.h \
		tile_map.h

//FORMS    += mainwindow.ui
