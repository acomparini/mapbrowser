#include "map_browser.h"
#include <QtOpenGL/QGLWidget>
#include <Qapplication>
#include "constants.h"
#include "tile_map.h"
#include <QPropertyAnimation>
#include <math.h>

map_browser::map_browser(): QGraphicsView(), _x0(0), _y0(0) {
//	setViewport(new QGLWidget);
	QGraphicsScene* scene = new QGraphicsScene;
	tm = new tile_map;
	scene->addItem(tm);
	setScene(scene);
	setDragMode(QGraphicsView::ScrollHandDrag);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	_anim = new QPropertyAnimation(this, "position");
	setMouseTracking(true);
}

map_browser::~map_browser(){

}

void map_browser::set_origin(double x, double y)
{
	_x0 = x;
	_y0 = y;
}
void map_browser::set_scale(int k)
{
    tm->set_scale(k);
}

void map_browser::get_tile_list()
{
    //tm->set_scale(_zm);

	QRect qr = viewport()->rect();
	int ntx = qr.width() / TILESZ + 2;
	int nty = qr.height() / TILESZ + 2;

    int zm = tm->get_scale();
    int cx = Longitude2TileX(_x0, zm);
    int cy = Latitude2TileY(_y0, zm);
//	double m1x = tm->lonToMerc(_x0);
//	double m1y = tm->latToMerc(_y0);
//	QPoint p10 = mapFromScene(m1x, m1y);
	
//    double lo0 = TileX2Longitude(cx, zm);
//    double la0 = TileY2Latitude(cy, zm);
//	double mx = tm->lonToMerc(lo0);
//	double my = tm->latToMerc(la0);
//	QPoint p0 = mapFromScene(mx, my);

//	int nx0 = cx - (int) ceil((double) p0.x() / TILESZ);
//	int nx1 = cx + (int) ceil((double) (qr.width() - (TILESZ + p0.x())) / TILESZ);

//	int ny0 = cy - (int) ceil((double) p0.y() / TILESZ);
//	int ny1 = cy + (int) ceil((double) (qr.height() - (TILESZ + p0.y())) / TILESZ);

	//std::set<QString> newtile;

	tm->clear_tile();
	setTransform(tm->transform());
	centerOn(tm->lonToMerc(_x0), tm->latToMerc(_y0));

    //tm->add_tile(cx, cy);

	//for ( int i = ny0; i <= ny1; i++) {
	//	for ( int j = nx0; j <= nx1; j++) {
	//		tm->add_tile(j, i);
	//	}
	//}

    for ( int i = cy - nty/2; i <= cy + nty/2; i++) {
        for ( int j = cx - ntx/2; j <= cx + ntx/2; j++) {
            tm->add_tile(j, i);
        }
    }
}

/*
void map_browser::wheelEvent(QWheelEvent * event)
{
	QPoint numPixels = event->angleDelta();
    if ( numPixels.y() > 0 && _zm < 18 )
		_zm += 1;
    if ( numPixels.y() < 0 && _zm > 0 )
		_zm -= 1;

	get_tile_list();
	update();
}
*/
void map_browser::resizeEvent(QResizeEvent* event)
{
    get_tile_list();
    update();
}

void map_browser::mouseDoubleClickEvent(QMouseEvent *event ) {
    QGraphicsView::mouseDoubleClickEvent(event);
    Qt::KeyboardModifiers km = QApplication::keyboardModifiers();
    int zm = tm->get_scale();
    if ( km == Qt::ShiftModifier && zm > 0 )
        zm--;
    else if ( zm < 19 )
        zm++;
    else
        return;
    //_zm %= 20;
    tm->set_scale(zm);

    get_tile_list();
    update();
}


void map_browser::scrollContentsBy(int dx, int dy)
{
	QGraphicsView::scrollContentsBy(dx, dy);
	//if ( tm == NULL )
	//	return;
	//QRect qr = viewport()->rect();

	//QPoint p(qr.width()/2, qr.height()/2);
	//p.setX(p.x() + dx);
	//p.setY(p.y() + dy);
	//QPointF pf = mapToScene(p);
	//_x0 = tm->MercTolon(pf.x());
	//_y0 = tm->MercTolat(pf.y());
	//get_tile_list();
	//update();
}

//void map_browser::paintEvent(QPaintEvent * event)
//{
//	//get_tile_list();
//	QGraphicsView::paintEvent(event);
//}

void map_browser::mousePressEvent( QMouseEvent * event )
 {
     if ( dragMode() == QGraphicsView::ScrollHandDrag ) {
        QPoint p = event->pos();
        _anim->stop();
         QGraphicsView::mousePressEvent(event);
         return;
     }

     if ( event->buttons() & Qt::LeftButton ) {
        QPoint p = event->pos();

        //centerOn(pf);
     }
     if ( event->buttons() & Qt::RightButton ) {
         Qt::KeyboardModifiers km = QApplication::keyboardModifiers();

         qreal angle = 5;
         if ( km == Qt::ShiftModifier )
            angle *= -1;
         QTransform qt = transform();
         qt.rotate(angle);
         setTransform(qt);
     }
 }

void map_browser::set_pos(QPointF pf)
{
	_x0 = pf.x();
	_y0 = pf.y();
	get_tile_list();
	update();

}
QPointF map_browser::get_pos()
{
	return QPointF(_x0, _y0);
}

void map_browser::mouseMoveEvent( QMouseEvent * event )
{
    QRect qr = viewport()->rect();
    QPoint p(qr.width()/2, qr.height()/2);
    QPointF pf = mapToScene(p);
    _xp = tm->MercTolon(pf.x());
    _yp = tm->MercTolat(pf.y());

	_pos.add( Position(QTime::currentTime(), QPointF(event->pos().x(), event->pos().y()) ) );
    QGraphicsView::mouseMoveEvent(event);
}

 void map_browser::mouseReleaseEvent( QMouseEvent * event )
{
     if ( dragMode() == QGraphicsView::ScrollHandDrag ) {
        QRect qr = viewport()->rect();

        QPoint p0(qr.width()/2, qr.height()/2);
        QPointF pf0 = mapToScene(p0);
        pf0 = QPointF(tm->MercTolon(pf0.x()), tm->MercTolat(pf0.y()));
        //double dx = _x0 - _xp;
        //double dy = _y0 - _yp;

        double ms = 1000;

        double acc = 0.0008;
        QPointF dis = _pos.displacementFromAcc(acc, ms);

        //QPointF dis = _pos.displacementFromTime(ms);

        QPoint p1 = QPoint(dis.x(), dis.y());
		QPointF pf1 = mapToScene(p0 - p1);
        pf1 = QPointF(tm->MercTolon(pf1.x()), tm->MercTolat(pf1.y()));

        _anim->setDuration(ms);
        _anim->setStartValue(pf0);
        _anim->setEndValue(pf1);
        _anim->setEasingCurve(QEasingCurve::OutQuad);
        _anim->start();
     }
     QGraphicsView::mouseReleaseEvent(event);
}
