#include "tile_map.h"
#include "tile_item.h"
#define  _USE_MATH_DEFINES
#include <math.h>
#include "constants.h"
#include <QPainter>
#include <qDir>
#include <QFileInfo>
#include <QGraphicsScene>

tile_map::tile_map(): QGraphicsItem(), _zm(0) {
}
tile_map::tile_map(int k): QGraphicsItem()  {
	set_scale(k);
}
int tile_map::get_scale() const {
    return _zm;
}

void tile_map::set_scale(int k) {
	_zm = k;
	_red = pow(2, _zm) / ( 2 * M_PI * RADIUS);
	double hext = (lonToMerc(lonmin) - lonToMerc(-lonmin)); // estensione orizzontale
	double vext = (latToMerc(latmin) - latToMerc(-latmin));	// estensione verticale
	double den = pow(2, _zm);

	double sdx = hext / den;

	double ll0 = lonToMerc(-lonmin);
	double la0 = latToMerc(-latmin);
	_qf = QRectF(ll0, la0, hext, vext);
	_qt.setMatrix(TILESZ / sdx, 0., 0., 0., -TILESZ / sdx, 0., 0, 0, 1.);
}
QRectF tile_map::boundingRect() const {
	return _qf;
}
void tile_map::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget * widget) {
    //painter->drawPixmap(0, 0, *_pix);
}
void tile_map::clear_tile() {
	QList<QGraphicsItem *> qli = childItems();
	QList<QGraphicsItem *>::iterator itl = qli.begin();
	for ( ; itl != qli.end(); itl++)
			scene()->removeItem(*itl);
}

void tile_map::add_tile(int tx, int ty) {
	
	tile_item* gt = new tile_item(this, _zm, tx, ty);
	if ( gt->is_valid() )
		scene()->addItem(gt);
	else
		delete gt;
}
QTransform tile_map::transform() const {
	return _qt;
}
double tile_map::lonToMerc(double lon) const {
	return _red * RADIUS * DEG_RAD(lon);
}
double tile_map::latToMerc(double lat) const {

	return _red * RADIUS * log( tan(DEG_RAD(lat)) + 1. / cos(DEG_RAD(lat)) );
}
double tile_map::MercTolon(double x) const {
	return RAD_DEG (x / (_red * RADIUS));
}
double tile_map::MercTolat(double y) const {
	return RAD_DEG(atan(sinh(y / (_red * RADIUS))));
}
