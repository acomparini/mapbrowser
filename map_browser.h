#ifndef MAPBROWSER_H
#define MAPBROWSER_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QTime>
#include <math.h>

class tile_map;
class QPropertyAnimation;

class Position {
public:
	Position() {}
	Position(const QTime& t, const QPointF& p): tm(t), pf(p) {}
	QPointF pf;
	QTime	tm;
};

class PositionList {
public:
	void add(const Position& p) {
		if ( _pos_list.size() >= 5 )
			_pos_list.pop_front();
		_pos_list.push_back(p);
	}
    QPointF displacementFromTime(double tm) {
		QPointF v = _velocity();

        double vmod = fmax(fabs(v.x()), fabs(v.y())) + 0.25 * fmin(fabs(v.x()), fabs(v.y()));
        double a = vmod / tm;

        return v * (0.5 * tm) ;
	}
    QPointF displacementFromAcc(double a, double& tm) {
        QPointF v = _velocity();
        double vmod = fmax(fabs(v.x()), fabs(v.y())) + 0.25 * fmin(fabs(v.x()), fabs(v.y())); //sqrt(v.x() * v.x() + v.y() * v.y());
        tm = vmod / a;
        return v * (0.5 * tm);
    }

private:
	QPointF _velocity(void) {
		if ( _pos_list.size() < 2 )
			return QPointF(0, 0);

		Position& p0 = _pos_list.front();
		Position& p1 = _pos_list.back();
		double tm = p0.tm.msecsTo(p1.tm);
		if ( tm == 0 )
			return QPointF(0, 0);
		return QPointF( (p1.pf.x() - p0.pf.x()) / tm, (p1.pf.y() - p0.pf.y()) / tm);

	}
	std::list<Position> _pos_list;
};

class map_browser: public QGraphicsView {

    Q_OBJECT
    Q_PROPERTY(QPointF position READ get_pos WRITE set_pos);
public:
    map_browser();
    virtual ~map_browser();

    void mousePressEvent( QMouseEvent * event );
    void mouseReleaseEvent( QMouseEvent * event );
    void mouseMoveEvent( QMouseEvent * event );
    void mouseDoubleClickEvent(QMouseEvent *event );
    void resizeEvent(QResizeEvent* event);
    //void wheelEvent(QWheelEvent * event);
	void scrollContentsBy(int dx, int dy);
//	void paintEvent(QPaintEvent  *event);
	void get_tile_list();
    void set_origin(double x, double y);
    void set_scale(int k);
	void set_pos(QPointF pf);
	tile_map* tm;
	QPointF get_pos();
private:
	std::map<QString, QGraphicsItem*> _oldtile;

	PositionList _pos;
	double _xp, _yp;

	double _x0, _y0;
	QPropertyAnimation*	_anim;
};

#endif // MAPBROWSER_H
