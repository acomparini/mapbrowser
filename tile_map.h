#ifndef TILEMAP_H
#define TILEMAP_H

#include <QGraphicsItem>

class tile_map: public QGraphicsItem {
public:
	tile_map();
	tile_map(int k);
	void set_scale(int k);
    int get_scale(void) const;
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget * widget = 0);
	void clear_tile();
	void add_tile(int tx, int ty);
	QTransform transform() const;
	double lonToMerc(double lon) const;
	double latToMerc(double lat) const;
	double MercTolon(double x) const;
	double MercTolat(double y) const;
	void update_tile_list();
private:
	int _zm;
	double _red;
	QRectF _qf;
	QTransform  _qt;
};
#endif
