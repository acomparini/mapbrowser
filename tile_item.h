#ifndef TILEITEM_H
#define TILEITEM_H

#include <QGraphicsItem>

class tile_map;

class tile_item: public QGraphicsItem {
public:
	tile_item();
    tile_item(tile_map* tm, int k, int tx, int ty);
    virtual ~tile_item();
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget * widget = 0);
	bool is_valid(void) const;
private:
    QPixmap* _pix;
    double _x, _y;
	QString _nome;
};
#endif
