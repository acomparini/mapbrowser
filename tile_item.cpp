#include "tile_item.h"
#include <Qstring>
#include <QPainter>
#include "constants.h"
#include <Qdir>
#include "tile_map.h"
#include <math.h>

tile_item::tile_item(): QGraphicsItem(), _x(0.), _y(0.), _pix(NULL) {
}

tile_item::tile_item(tile_map* tm, int k, int tx, int ty): QGraphicsItem(), _x(0.), _y(0.), _pix(NULL) {
#ifdef WIN32
    QDir dir("E:/Temp/mycache/osm");
#else
    QDir dir("/Volumes/My Passport/MapCache/");
#endif

	QString qsz, qsx, qsy;
	QString qs = QString("_") + qsz.setNum(k) + QString("_") + qsx.setNum(tx) + QString("_") + qsy.setNum(ty) + QString(".png");
	QFileInfo qf(dir, qs);

	if ( qf.exists() ) {
		setParentItem(tm);

	    _pix = new QPixmap(qf.absoluteFilePath());

		double tlon0 = TileX2Longitude(tx, k);
		double tlat0 = TileY2Latitude(ty, k);
		_x = tm->lonToMerc(tlon0);
		_y = tm->latToMerc(tlat0);
		setPos(_x, _y);

		double dpx = 2 * lonmin / pow(2, k);
		double dx = tm->lonToMerc(dpx) / TILESZ;
		QTransform qt(dx, 0, 0, 0, -dx, 0, 0, 0, 1);
		setTransform(qt);
	}

	_nome = qs;
}

tile_item::~tile_item() {
    if (_pix != NULL )
        delete _pix;
    _pix = NULL;
}

QRectF tile_item::boundingRect() const {
	return QRectF(0, 0, TILESZ, TILESZ);
}
void tile_item::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget * widget) {
    painter->drawPixmap(0, 0, *_pix);
	painter->drawRect(0, 0, TILESZ, TILESZ);
}

bool tile_item::is_valid(void) const
{
	return _pix != NULL;
}

//void tile_item::set_pos(double x, double y, double x1, double y1) {
//    _x = x;
//    _y = y;
//	double dx = x1 - _x;
//	double dy = _y - y1;
//	QTransform qt(dx/TILESZ, 0, 0, 0, -dx/TILESZ, 0, 0, 0, 1);
//	//QTransform qt(dpx/TILESZ, 0, 0, 0, -dpx/TILESZ, 0, 0, 0, 1);
//	setTransform(qt);
//	setPos(_x, _y);
//}
